<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function iphoto_image_imagecache_default_presets() {
  $items = array(
    'iphoto_preview' => array(
      'presetname' => 'iphoto_preview',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '800',
            'height' => '800',
            'upscale' => 0,
          ),
        ),
      ),
    ),
    'iphoto_tn' => array(
      'presetname' => 'iphoto_tn',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '200',
            'height' => '200',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function iphoto_image_node_info() {
  $items = array(
    'iphoto_image' => array(
      'name' => t('iphoto_image'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Caption'),
      'has_body' => '1',
      'body_label' => t('Comment'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
