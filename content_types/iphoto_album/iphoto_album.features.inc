<?php

/**
 * Implementation of hook_node_info().
 */
function iphoto_album_node_info() {
  $items = array(
    'iphoto_album' => array(
      'name' => t('iphoto_album'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('AlbumName'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
