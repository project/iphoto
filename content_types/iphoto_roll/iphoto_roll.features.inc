<?php

/**
 * Implementation of hook_node_info().
 */
function iphoto_roll_node_info() {
  $items = array(
    'iphoto_roll' => array(
      'name' => t('iphoto_roll'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('RollName'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
