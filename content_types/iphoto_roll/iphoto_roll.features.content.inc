<?php

/**
 * Implementation of hook_content_default_fields().
 */
function iphoto_roll_content_default_fields() {
  $fields = array();

  // Exported field: field_iphoto_roll_keyphotokey
  $fields['iphoto_roll-field_iphoto_roll_keyphotokey'] = array(
    'field_name' => 'field_iphoto_roll_keyphotokey',
    'type_name' => 'iphoto_roll',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'iphoto_image' => 'iphoto_image',
      'page' => 0,
      'story' => 0,
      'iphoto_album' => 0,
      'iphoto_roll' => 0,
    ),
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'KeyPhotoKey',
      'weight' => '27',
      'description' => '',
      'type' => 'nodereference_buttons',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_iphoto_roll_photocount
  $fields['iphoto_roll-field_iphoto_roll_photocount'] = array(
    'field_name' => 'field_iphoto_roll_photocount',
    'type_name' => 'iphoto_roll',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_iphoto_roll_photocount][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'PhotoCount',
      'weight' => '28',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_iphoto_roll_rollid
  $fields['iphoto_roll-field_iphoto_roll_rollid'] = array(
    'field_name' => 'field_iphoto_roll_rollid',
    'type_name' => 'iphoto_roll',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_iphoto_roll_rollid][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'RollID',
      'weight' => '26',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('KeyPhotoKey');
  t('PhotoCount');
  t('RollID');

  return $fields;
}
