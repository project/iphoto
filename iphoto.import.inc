<?php

function iphoto_deleteall() {
  $node_types = array(
    'iphoto_image', 
    'iphoto_album', 
    'iphoto_roll', 
  );

  $sql = 'SELECT nid FROM {node} WHERE type IN ('. implode(', ', array_fill(0, count($node_types), "'%s'")). ')';
  $result = db_query($sql, $node_types);
  $operations = array();
  
  while ($row = db_fetch_object($result)) {
    // TODO: this can be too slow, find a batch style alternative    
    node_delete($row->nid);
    //$deleted_count ++;
  }
  
  //simple debug message so we can see what had been deleted.
  //drupal_set_message("$deleted_count nodes have been deleted");
}


function iphoto_import_albumdata_xml() {
  // delete all nodes first. EEEEK
  iphoto_deleteall();
  /*
  $operations[] = array('iphoto_deleteall', array());
  
  // Set up the Batch API
  $batch = array(
    'operations' => $operations,
    'finished' => '',
    'title' => t('Importing iPhoto Album Data'),
  );
  
  batch_set($batch);
  batch_process();  
  unset($batch);  
  unset($operations);
  */
  
  $iphoto_albumdata_array = iphoto_get_albumdata_xml();

  // process all albums and save each one as a node
  $albums_array = $iphoto_albumdata_array->data['iphoto_master_array']['List of Albums'];
  $master_image_list = $iphoto_albumdata_array->data['iphoto_master_array']['Master Image List'];
  
  foreach ($albums_array as $album_array) {
    // create album nodes
    $album_node_obj = iphoto_save_albumnode($album_array);
    //dpm($album_node_obj);
    // TODO: return some neat user friendly output with list of albums and images created
    
    // loop through each of the images assigned to album and create image nodes
    $album_images_array = $album_array['KeyList'];
    
    foreach ($album_images_array as $album_image_key) {
      $album_image_array = $master_image_list[$album_image_key];
      //$operations[] = array('iphoto_save_imagenode', array($album_image_array, $album_node_obj->nid));
      $image_node_obj = iphoto_save_imagenode($album_image_array, $album_node_obj->nid);
      
      // TODO: return some neat user friendly output with list of albums and images created
      //echo '<li>'. $album_image_array['Caption'] .'</li>';
    }
  }
  
  /*
  // Set up the Batch API
  $batch = array(
    'operations' => $operations,
    'finished' => '',
    'title' => t('Importing iPhoto Album Data'),
  );
  
  batch_set($batch);
  
  batch_process();  
  */
  
  return 'done';
}


function iphoto_save_imagenode($image_array, $album_nid) {
  $path = preg_replace('/^(.*)Previews/', '', $image_array['ImagePath']);

  //$thumbnail_path = file_create_path('Thumbnails'. $path);
  $preview_path = variable_get('iphoto_previews_path', file_directory_path() .'/iphoto/Previews') . $path;

  $image_node_obj = new StdClass;
  
  // add node properties
  $image_node_obj->type = 'iphoto_image';
  $image_node_obj->title = $image_array['Caption'];
  $image_node_obj->body = $image_array['Comment'];
  $image_node_obj->uid = 1;
  
  // TODO: find out a way to format iPhoto time strings
  $image_node_obj->created = strtotime("now"); //$image_array['DateAsTimerInterval'];
  $image_node_obj->changed = strtotime("now"); //$image_array['ModDateAsTimerInterval'];
  $image_node_obj->status = 1;
  $image_node_obj->comment = 0;
  $image_node_obj->promote = 0;
  $image_node_obj->moderate = 0;
  $image_node_obj->sticky = 0;

  // CCK
  $image_node_obj->field_iphoto_image_date[0]['value'] = $image_array['DateAsTimerInterval'];
  $image_node_obj->field_iphoto_image_moddate[0]['value'] = $image_array['ModDateAsTimerInterval'];
  $image_node_obj->field_iphoto_image_mediatype[0]['value'] = $image_array['MediaType'];
  $image_node_obj->field_iphoto_image_guid[0]['value'] = $image_array['GUID'];
  $image_node_obj->field_iphoto_image_rating[0]['value'] = $image_array['Rating'];
  $image_node_obj->field_iphoto_image_imagepath[0]['value'] = $preview_path;
  $image_node_obj->field_iphoto_image_album[0]['nid'] = $album_nid;
  
  node_save($image_node_obj);
  
  return $image_node_obj;
}


function iphoto_save_albumnode($album_array) {
  $album_node_obj = new StdClass;
  
  // add node properties
  $album_node_obj->type = 'iphoto_album';
  $album_node_obj->title = $album_array['AlbumName'];
  $album_node_obj->uid = 1;
  $album_node_obj->created = strtotime("now");
  $album_node_obj->changed = strtotime("now");
  $album_node_obj->status = 1;
  $album_node_obj->comment = 0;
  $album_node_obj->promote = 0;
  $album_node_obj->moderate = 0;
  $album_node_obj->sticky = 0;
  
  // cck
  $album_node_obj->field_iphoto_album_albumid[0]['value'] = $album_array['AlbumId'];    
  $album_node_obj->field_iphoto_album_guid[0]['value'] = $album_array['GUID'];    
  $album_node_obj->field_iphoto_album_photocount[0]['value'] = $album_array['PhotoCount'];

  // save node
  node_save($album_node_obj);
  
  return $album_node_obj;
}


function iphoto_get_albumdata_xml() {
  $iphoto_albumdata_array = cache_get('iphoto_albumdata');
  
  if (empty($iphoto_albumdata_array)) {
    // rebuild 
    iphoto_parse_albumdata_xml();
    
    //try again
    $iphoto_albumdata_array = iphoto_get_albumdata_xml();
    
    //TODO: not sure if this is a good idea to keep looping
  }

  return $iphoto_albumdata_array;  
}


function iphoto_parse_albumdata_xml() {
  $albumdata_file = file_create_path(variable_get('iphoto_albumdata_path', ''));
  
  if (!file_exists($albumdata_file)) {
    // TODO: add watchdog entry & set message
    return;
  }
  // TODO: check if file is newer than cache time, if so, rebuild
  
  // Require CFPropertyList
  require(variable_get('iphoto_cfpropertylist_path', 'sites/all/libraries/CFPropertyList/CFPropertyList.php'));
  
  // create a new CFPropertyList instance which loads the sample.plist on construct.
  // since we know it's an XML file, we can skip format-determination
  $plist = new CFPropertyList($albumdata_file, CFPropertyList::FORMAT_XML);
  
  // retrieve the array structure from iphoto XML
  $iphoto_master_array = $plist->toArray();
  
  $rolls_array = $iphoto_master_array['List of Rolls'];
  
  // rebuild rolls array to contain roll id as the key
  foreach ($rolls_array as $roll_array) {
    unset($roll_array['KeyList']);
    $rolls_array_new[$roll_array['RollID']] = $roll_array;
  }
  
  $images_array = $iphoto_master_array['Master Image List'];
  
  // change the thumbnail and preview paths so symlinks are relative
  foreach ($images_array as $image_key => $image_value) {
    $path = preg_replace('/^(.*)Previews/', '', $image_value['ImagePath']);
    
    //$thumbnail_path = file_create_path('Thumbnails'. $path);
    $preview_path = variable_get('iphoto_previews_path', file_directory_path() .'/iphoto/Previews') . $path;
  
    //$images_array[$image_key]['thumbnail_path'] = $thumbnail_path;
    $images_array[$image_key]['preview_path'] = $preview_path;
    
    // if the image belongs to a roll, add it to rolls array
    if (array_key_exists($image_value['Roll'], $rolls_array_new)) {
      $rolls_array_new[$image_value['Roll']]['images'][$image_key] = $images_array[$image_key];
    }
  }
  
  $iphoto_albumdata_array = array(
    'iphoto_master_array' => $iphoto_master_array, 
    'rolls_array' => $rolls_array_new, 
    'images_array' => $images_array,
  );
  
  cache_set('iphoto_albumdata', $iphoto_albumdata_array);
  
  return 'Rebuilt cache with iPhoto AlbumData.xml';
}