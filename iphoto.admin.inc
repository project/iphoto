<?php

function iphoto_settings() {
  $form['iphoto_cfpropertylist_path'] = array(
    '#type' => 'textfield',
    '#title' => t('CFPropertyList Library'),
    '#default_value' => variable_get('iphoto_cfpropertylist_path', 'sites/all/libraries/CFPropertyList/CFPropertyList.php'),
    '#description' => t('Please ensure you have downloaded and copied the CFPropertyList library to the above mentioned path.'),
    // TODO: add a check to ensure lib exists
    
  );
  
  
  // TODO: create the default folder for AlbumData.xml
  $form['iphoto_albumdata_path'] = array(
    '#type' => 'textfield',
    '#title' => t('AlbumData.xml File'),
    '#default_value' => variable_get('iphoto_albumdata_path', file_directory_path() .'/iphoto/AlbumData.xml'),
    '#description' => t('Please ensure you have uploaded AlbumData.xml.'),
    
    // TODO: add a check to ensure file exists
  );

  // TODO: create the default folder for Previews
  $form['iphoto_previews_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Previews folder'),
    '#default_value' => variable_get('iphoto_previews_path', file_directory_path() .'/iphoto/Previews'),
    '#description' => t('Please ensure you have uploaded Previews folder'),
    
    // TODO: add a check to ensure file exists
  );
  
  return system_settings_form($form);
  
}
